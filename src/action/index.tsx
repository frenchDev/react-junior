import React from 'react'
import axios from 'axios'

export async function getDataEnergy(path: string): Promise<any> {
  let result
  try {
    result = await axios.get(
      `https://5e9ed3cdfb467500166c47bb.mockapi.io/api/v1/meter/${path}`
    )
  } catch (rej) {
    return console.log('error:', rej)
  }

  return result.data
}


