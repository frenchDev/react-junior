import React from 'react'
import './Header.scss'
import Logo from '../assets/logo-ekWateur.png'

const Header = () => {
  return (
    <div data-test="component-header">
      <img
        src={Logo}
        data-test="header-logo"
        alt="Logo ekWateur for green energy"
      />
    </div>
  )
}

export default Header
