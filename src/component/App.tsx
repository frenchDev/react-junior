import React from 'react'
import './App.scss'
import Header from './Header'
import TableEnergy from './TableEnergy'

const App = () => {
  return (
    <div data-test="component-app" className="container">
      <Header />
      <h1>Your dashboard for energy </h1>
      <TableEnergy />
    </div>
  )
}

export default App
