import React from 'react'
import axios from 'axios'
import moxios from 'moxios'
import { shallow, mount } from 'enzyme'
import TableEnergy from './TableEnergy'

describe('getDataEnergy action creator', () => {
  beforeEach(() => {
    moxios.install()
  })
  afterEach(() => {
    moxios.uninstall()
  })
  test('receive response from query Axios', async () => {
    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 200,
      })
    })
    let result = await axios.get(
      'https://5e9ed3cdfb467500166c47bb.mockapi.io/api/v1/meter/2/electricity'
    )
    expect(result).not.toBe('')
  })
})
describe('renders table energy', () => {
  test('renders without error', async () => {
    const wrapper = shallow(<TableEnergy />)
    expect(wrapper.html()).toBe('<p data-test="loader">Loading...</p>')
  })
  test('display table after loading', async () => {
    const wrapper = shallow(<TableEnergy />)
    expect(wrapper.find(`[data-test='loader']`)).toHaveLength(1)
    await Promise.resolve()
    expect(wrapper.find(`[data-test='component-table-energy']`)).toHaveLength(1)
  })
})
