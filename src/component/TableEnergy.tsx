import React, { useState, useEffect } from 'react'
import axios from 'axios'

interface Gas {
  meter: { id: string; createdAt: string; pointOfDelivery: string }
  meterId: string
  createdAt: string
  indexHigh: number
}
interface Electricity {
  id: string
  meterId: string
  createdAt: string
  indexHigh: number
  indexLow: number
}
const ascending = function (a: any, b: any) {
  a = new Date(a.createdAt)
  b = new Date(b.createdAt)
  return a > b ? -1 : a < b ? 1 : 0
}
const descending = function (a: any, b: any) {
  a = new Date(a.createdAt)
  b = new Date(b.createdAt)
  return a > b ? -1 : a < b ? 1 : 0
}

const TableEnergy = () => {
  const [data, setData] = useState([] as any)
  const [loading, setLoading] = useState(true)
  const [energy, setEnergy] = useState('electricity')
  const [sortFilter, setSortFilter] = useState('ascending')

  useEffect(() => {
    if (energy === 'electricity') {
      axios
        .get<Electricity[]>(
          'https://5e9ed3cdfb467500166c47bb.mockapi.io/api/v1/meter/2/electricity'
        )
        .then((response) => {
          if (sortFilter === 'ascending') {
            setData(response.data.sort(ascending))
          } else {
            setData(response.data.sort(descending))
          }

          setLoading(false)
        })
        .catch((error) => console.log(error))
    } else {
      axios
        .get<Gas[]>(
          'https://5e9ed3cdfb467500166c47bb.mockapi.io/api/v1/meter/1/gas'
        )
        .then((response) => {
          if (sortFilter === 'ascending') {
            setData(response.data.sort(ascending))
          } else {
            setData(response.data.sort(descending))
          }
          setLoading(false)
        })
        .catch((error) => console.log(error))
    }
  }, [energy, sortFilter])

  if (loading) {
    return <p data-test="loader">Loading...</p>
  }

  const energyRows = data.map((item: any) => (
    <tr data-test="index-values" key={item.id}>
      <td className="reverseTd">
        {item.createdAt.split('T')[0].replaceAll('-', '/')}
      </td>
      <td>{item.indexHigh}</td>
      {energy === 'electricity' ? <td>{item.indexLow}</td> : null}
    </tr>
  ))
  const content: any = (
    <div data-test="tab-values">
      <h3>Select energy type</h3>
      <button
        data-test="button-energy"
        className="btn btn-orange"
        aria-label="button select electricity"
        onClick={() => setEnergy('electricity')}
      >
        electricity
      </button>
      <button
        data-test="button-energy"
        className="btn btn-orange"
        aria-label="button select electricity"
        onClick={() => setEnergy('gas')}
      >
        gas
      </button>

      <select id="select-sort" onChange={(e) => setSortFilter(e.target.value)}>
        <option value="ascending">Ascending</option>
        <option value="descending">Descending</option>
      </select>

      <table className="table-index">
        <caption>
          <h2>Type of energy: {energy}</h2>
          <p>Point of delivery: {data[0].meter.pointOfDelivery}.</p>
        </caption>

        <thead className="thead-light">
          <tr>
            <th>Period</th>
            <th>Index high</th>
            {energy === 'electricity' ? <th>Index Low</th> : null}
          </tr>
        </thead>
        {/**display the index value for each period  */}
        <tbody>{energyRows}</tbody>
      </table>
    </div>
  )

  return <div data-test="component-table-energy">{content}</div>
}

export default TableEnergy
